# Welcome !

## About the Application: 
This repository contains a simple REST CRUD API written in nodejs and express , uses mongodb as DB and deployed in a docker swarm.

Besides having exposed course endpoints(please see relative postman collection under endpoints folder in the repo), this app is monitored alongside with it"s server using Prometheus and node-exporter and has relative dashboard done using Grafana.

# How to use the deployed application ?
## How to consume courseAPI endpoints ?

Please find attached in the repository a postman collection of all enpoints that can be used.

## How to monitor the application and nodes deployed on ?

Please contact me with my mail and I will create an account for you and send your credentials.
# Dashboard link : 
```
http://3.208.211.117:3000/
```
# How to use the application for development ?
### Prerequisites: 
```
npm 6+,nodejs 11+,mongoDB server
```
### -Clone the repo
### -Install dependencies: 
```bash
npm install 
```
### -Run the application on your local environment
```bash
npm run dev
```
# How to deploy the application locally using docker ?
### Prerequisites: 
```
docker engine, docker compose 
```
### -Clone the repo
### -cd to the app where you find dockerfile and docker-compose.yml files.
```bash
cd [path to the app]
```
### -To deploy on a single node:
```bash
docker-compose . up
```
### -To deploy on a cluster using docker stack :
```bash
docker stack deploy -c docker-compose.yml [nameOfTheStack]
```
### Notice: 
To deploy on a swarm you must have a created swarm and be in the master node.


const express = require("express");
const router = express.Router();

// App
const client = require('prom-client');
const collectDefaultMetrics = client.collectDefaultMetrics;
// Probe every 5th second.
collectDefaultMetrics({ timeout: 5000 });

const counter = new client.Counter({
  name: 'node_request_operations_total',
  help: 'The total number of processed requests'
});

const histogram = new client.Histogram({
  name: 'node_request_duration_seconds',
  help: 'Histogram for the duration in seconds.',
  buckets: [1, 2, 5, 6, 10]
});


router.get('/',async(req,res)=>{
  const appMetrics= await client.register.metrics();
  console.log(appMetrics);
  res.contentType = client.register.contentType;
  return res.status(200).send(appMetrics);
});

module.exports = router;






FROM node:14.15-alpine
WORKDIR /usr/src/node_mongo_docker_compose/
ENV SYSDIG_AGENT_CONF 'app_checks: [{name: node, check_module: prometheus, pattern: {comm: node}, conf: { url: "http://localhost:{port}/metrics" }}]'
COPY package.json /usr/src/node_mongo_docker_compose
RUN npm install
COPY . /usr/src/node_mongo_docker_compose
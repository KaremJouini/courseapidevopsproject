const request = require("supertest");
const express = require("express");
const app = express();
var courseID; //to save the id of the created course to delete it after creation


describe("POST Create Course", () => {
  it("should create course for the user", () => {
    // code for testing the api
    const newCourse = {
      title: "Devops",
      faculty: "GL",
      level: "5",
      teacherName: "Maher Assel",
    };
    request(app)
      .post("course")
      .send(newCourse)
      .expect(200)
      .then((res) => {
        courseID = res.body.data;
        expect(res.body.data).to.be.eql(newCourse);
        // more validations can be added here as required
      });
  });
});

describe("DELETE  Delete Course", () => {
  it("should delete course for the user", () => {
    request(app)
      .delete("course/" + courseID)
      .expect(200)
      .then((res) => {
        expect(res.body.data.id).to.be.eql(courseID);
        // more validations can be added here as required
      });
  });
});
